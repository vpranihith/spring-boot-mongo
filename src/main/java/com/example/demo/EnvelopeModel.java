package com.example.demo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "envelopes")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EnvelopeModel {

    @Id
    private String id;

    private String name;
    private int quantity;
    private String category;

    @Version
    private Long version;

}
