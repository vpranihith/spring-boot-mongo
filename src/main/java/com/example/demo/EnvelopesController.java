package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EnvelopesController {

    @Autowired
    private EnvelopeRepository er;

    @Autowired
    private EnvelopeService es;

    @GetMapping("/envelopes")
    public List<EnvelopeModel> getEnvelopes() {
        return es.getEnvelopes();
    }

    @GetMapping("/envelopes/name/{name}")
    public EnvelopeModel getEnvelopeByName(@PathVariable("name") String name) {
        return es.getEnvelopeByName(name);
    }

    @GetMapping("/envelopes/{id}")
    public EnvelopeModel getEnvelopeById(@PathVariable("id") String id) {
        return es.getEnvelopeById(id);
    }

    @PostMapping("/envelopes")
    public EnvelopeModel addEnvelope(@RequestBody EnvelopeModel envelopeModel) {
        return es.createEnvelope(envelopeModel);
    }

    @PutMapping("/envelopes")
    public EnvelopeModel updateEnvelope(@RequestBody EnvelopeModel envelopeModel) {
        return es.updateEnvelope(envelopeModel);
    }

    @DeleteMapping("/envelopes/{id}")
    public void delete(@PathVariable("id") String id) {
        es.deleteEnvelope(id);
    }

    @GetMapping("/hello")
    public String test() {
        return "Hi!";
    }

    @PostMapping("/envelopes/bulk/save")
    public void bulkSave() {
        long startTime = System.currentTimeMillis();
        es.bulkSave();
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("Bulk Save time in milliseconds: " + elapsedTime);
    }

    @PostMapping("/envelopes/bulk/update")
    public void bulkUpdate() {
        long startTime = System.currentTimeMillis();
        es.bulkUpdate();
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("Bulk Update time in milliseconds: " + elapsedTime);
    }
}
