package com.example.demo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface EnvelopeRepository extends MongoRepository<EnvelopeModel, String> {

    @Query("{name:'?0'}")
    EnvelopeModel findItemByName(String name);

    @Query(value="{category:'?0'}", fields="{'name' : 1, 'quantity' : 1}")
    List<EnvelopeModel> findAll(String category);

    public long count();

}
