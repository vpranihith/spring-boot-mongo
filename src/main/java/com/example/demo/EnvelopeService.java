package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class EnvelopeService {

    @Autowired
    private EnvelopeRepository er;

    public List<EnvelopeModel> getEnvelopes() {
        return this.er.findAll();
    }

    public EnvelopeModel getEnvelopeByName(String name) {
        return this.er.findItemByName(name);
    }

    public EnvelopeModel getEnvelopeById(String id) {
        Optional<EnvelopeModel> e = this.er.findById(id);
        if (e.isPresent()) {
            return e.get();
        }
        return null;
    }

    public EnvelopeModel createEnvelope(EnvelopeModel em) {
        EnvelopeModel savedModel = this.er.save(em);
        log.info("saved model {}", savedModel);
        return savedModel;
    }

    public void deleteEnvelope(String id) {
        EnvelopeModel modelToDelete = this.getEnvelopeById(id);
        if (modelToDelete != null) {
            this.er.delete(modelToDelete);
        }
    }

    public EnvelopeModel updateEnvelope(EnvelopeModel em) {
        Optional<EnvelopeModel> existingModel = this.er.findById(em.getId());
        if (existingModel.isPresent()) {
            EnvelopeModel updatedModel = existingModel.get();
            updatedModel.setQuantity(em.getQuantity());
            updatedModel.setName(em.getName());
            updatedModel.setCategory(em.getCategory());
            updatedModel.setVersion(em.getVersion());
            this.er.save(updatedModel);
            return this.getEnvelopeById(em.getId());
        }
        return this.er.save(em);
    }

    public void bulkSave() {

        this.bulkDelete();
        List<EnvelopeModel> em = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            String category = i % 2 == 0 ? "bulk" : "little";
            EnvelopeModel e = new EnvelopeModel(null, "envelope1" + i, i, category, null);
            em.add(e);
        }
        this.er.saveAll(em);
    }

    public void bulkDelete() {
        long startTime = System.currentTimeMillis();
        this.er.deleteAll();
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("Bulk Delete time in milliseconds: " + elapsedTime);
    }


    public void bulkUpdate() {
        List<EnvelopeModel> em = this.er.findAll();
        List<EnvelopeModel> allEm = new ArrayList<>();
        em.forEach(e -> {
            e.setQuantity(e.getQuantity() + 1);
            allEm.add(e);
        });
        this.er.saveAll(allEm);
    }
}
